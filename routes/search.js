/*
 * search.js
 */

const search = require('../models/search.js')


module.exports = function (app, opts, next) {

  app.get('/autocomplete/:term', function (request, reply) {
    const {term} = request.params
    const fetchAll = request.query.fetchAll === 'true'

    search.autocomplete(term, fetchAll)
    .then(reply.okHandler())
    .catch(reply.errorHandler())
  })

  app.get('/by-term/:term', function (request, reply) {
    const {term} = request.params

    search.byTerm(term)
    .then(reply.okHandler())
    .catch(reply.errorHandler())
  })

  next()
}
