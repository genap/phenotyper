

const fs = require('fs')
const path = require('path')

module.exports = function (fastify, opts, next) {
  fastify.get('/', function (request, reply) {
    reply.type('text/html')
    reply.send(fs.createReadStream(path.join(__dirname, '../public/index.html')))
  })

  next()
}
