/*
 * models.js
 */

import { groupBy } from 'rambda'


export function normalizeViccResult(item) {
  return {
    source: item._source.source,
    sourceURL: item._source.source_url,
    genes: item._source.genes,
    drugs: item._source.drugs,
    featureNames: item._source.feature_names,
    publications: item._source.publications,
    diseases: item._source.diseases,
    description: item.highlight['association.description'],
    evidenceLabel: item._source.evidence_label,
    response: item._source.response,
  }
}

export function processResults(result) {
  const results = result.hits.map(normalizeViccResult)
  const groupedResults =
    Object.entries(groupBy(r => r.genes.join(', '), results))
      .map(([genes, items]) => ({ genes, items }))
  groupedResults.sort(groupEvidenceLabelComparator)
  return groupedResults
}

function groupEvidenceLabelComparator(a, b) {
  const va = a.items.map(i => i.evidenceLabel).sort(stringComparator).join('')
  const vb = b.items.map(i => i.evidenceLabel).sort(stringComparator).join('')
  return va.localeCompare(vb)
}

function stringComparator(a, b) {
  return a.localeCompare(b)
}
