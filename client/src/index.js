import { render } from 'inferno';
import { initDevTools } from 'inferno-devtools';
import './styles/index.css';
import './polyfills';
import './helpers/detect-browser';
import App from './components/App';
import * as serviceWorker from './serviceWorker';

initDevTools()

render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
