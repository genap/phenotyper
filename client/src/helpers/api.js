/*
 * api.js
 */

import axios from 'axios'

import { processResults } from '../models'


const PREFIX = '/api'

const sources = new Map()


export const search = {
  autocomplete: (input, fetchAll = false) =>
    get(`/search/autocomplete/${input}?fetchAll=${fetchAll}`, { id: '/search/autocomplete', single: true }) ,
  byTerm: (term) => get(`/search/by-term/${term}`).then(processResults),
}




function fetch(route, options) {
  const url = PREFIX + route
  const id = options.id || (options.method + '__' + url)

  if (options.single && sources.has(id)) {
    const source = sources.get(id)
    source.cancel(new Error('Request canceled by next request'))
  }
  if (options.single) {
    const source = axios.CancelToken.source()
    sources.set(id, source)
    options = { ...options, cancelToken: options.single ? source.token : undefined }
  }

  return axios(url, options)
  .then(response => {
    if (options.single) {
      sources.delete(id)
    }
    return handleAPIResponse(response)
  })
}

function get(route, options) {
  return fetch(route, { method: 'GET', ...options })
}

function post(route, data, options) {
  return fetch(route, { method: 'POST', data, ...options })
}

function handleAPIResponse(response) {
  const apiResponse = response.data
  if (apiResponse.ok === false) {
    const e = new Error(apiResponse.message)
    e.message = apiResponse.message
    e.stack = apiResponse.stack
    return Promise.reject(e)
  }
  return apiResponse.data
}
