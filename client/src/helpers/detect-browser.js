/*
 * browser.js
 */


export const isFirefox = /Firefox/.test(navigator.userAgent)
export const isChrome  = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor)
export const isSafari  = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor)
export const isEdge    = /Edge/.test(navigator.userAgent)

export const isWebKit  = !!window.webkitURL

export const isMac     = navigator.platform.toLowerCase().includes('mac')

if (isMac)
  document.body.classList.add('os-mac')
else
  document.body.classList.add('os-unknown')

if (isChrome)
  document.body.classList.add('browser-chrome')
else if (isFirefox)
  document.body.classList.add('browser-firefox')
else if (isSafari)
  document.body.classList.add('browser-safari')
else if (isEdge)
  document.body.classList.add('browser-edge')
else
  document.body.classList.add('browser-unknown')
