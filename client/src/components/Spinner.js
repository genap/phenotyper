/*
 * Spinner.js
 */

import cx from 'classname'


function Spinner({ size, hidden, inverse, className }) {
  return (
    <div className={
      cx(
        'Spinner',
        className,
        size,
        {
          'Spinner--visible': !hidden,
          'Spinner--inverse': inverse,
        }
      )
    }/>
  )
}

export default Spinner
