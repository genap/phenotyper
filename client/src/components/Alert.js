/*
 * Alert.js
 */

import cx from 'classname'


function Alert({ children, success, info, warning, danger, size = 'medium' }) {

  const className = cx('Alert',
    size ? `Alert--${size}` : undefined,
    {
      'Alert--success': success,
      'Alert--info': info,
      'Alert--warning': warning,
      'Alert--danger': danger,
    })

  return (
    <div className={className}>
      {children}
    </div>
  )
}

export default Alert;
