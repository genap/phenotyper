/*
 * SearchBox.js
 */

import { Component } from 'inferno';
import axios from 'axios'

import Spinner from './Spinner'
import * as api from '../helpers/api'

class SearchBox extends Component {
  state = {
    isLoading: false,
    originalValue: '',
    inputValue: '',
    terms: undefined,
    termsTotalCount: undefined,
    selectedIndex: -1,
    autocomplete: false,
  }

  select(move) {
    const { terms, selectedIndex, originalValue } = this.state
    let newIndex = selectedIndex + move

    while (newIndex > (terms.length - 1)) {
      newIndex = newIndex - (terms.length + 1)
    }

    while (newIndex < -1) {
      newIndex = newIndex + (terms.length + 1)
    }

    this.setState({
      selectedIndex: newIndex,
      inputValue: newIndex === -1 ? originalValue : terms[newIndex].name,
    })
  }

  submit(index = this.state.selectedIndex) {
    const { terms } = this.state
    const term = index === -1 ? terms[0] : terms[index]
    if (!term)
      return
    this.props.onSubmit(term)
    this.setState({ autocomplete: false, inputValue: term.name })
  }

  fetchTerms(inputValue, fetchAll) {
    const searchValue = inputValue.trim()

    if (searchValue === this.lastSearchValue && !fetchAll) {
      return
    }

    if (searchValue === '') {
      this.setState({
        isLoading: false,
        selectedIndex: -1,
        terms: undefined,
        termsTotalCount: undefined,
        autocomplete: false,
      })
      return
    }

    this.lastSearchValue = searchValue
    this.setState({ isLoading: true })

    api.search.autocomplete(searchValue, fetchAll)
    .then(result => {
      this.setState({
        isLoading: false,
        selectedIndex: -1,
        terms: result.terms,
        termsTotalCount: result.total,
        autocomplete: true,
      })
    })
    .catch(e => {
      if (axios.isCancel(e))
        return
      // TODO(else?)
      this.setState({ isLoading: false })
    })
  }

  setInputValue = ev => {
    const inputValue = ev.target.value
    this.setState({ inputValue, originalValue: inputValue })
    this.fetchTerms(inputValue, false)
  }

  onKeyDown = (ev) => {
    if (ev.code === 'Tab') {
      if (!this.state.autocomplete)
        return
      if (ev.shiftKey) {
        this.select(-1)
      }
      else {
        this.select(+1)
      }
      ev.preventDefault()
    }
    else if (ev.code === 'Enter') {
      this.submit()
      ev.preventDefault()
    }
  }

  onClickLoadAll = () => {
    this.fetchTerms(this.state.inputValue, true)
  }

  render() {
    const {
      inputValue,
      autocomplete,
      terms,
      termsTotalCount,
      selectedIndex,
    } = this.state

    const isLoading = this.state.isLoading || this.props.isLoading

    return (
      <div className='SearchBox'>
        <div className='SearchBox__content'>
          <input
            type='text'
            className='SearchBox__input'
            value={inputValue}
            onInput={this.setInputValue}
            onKeyDown={this.onKeyDown}
          />
          <Spinner className='SearchBox__spinner' size='small' hidden={!isLoading} />
        </div>
        {
          autocomplete &&
            <div className='SearchBox__results'>
              {
                terms.map((term, i) =>
                <div
                  key={term.id}
                  role='button'
                  className={ 'SearchBox__results__row SearchBox__results__row--clickable'
                    + (selectedIndex === i ? ' SearchBox__results__row--selected' : '') }
                  onClick={() => this.submit(i)}
                >
                    <span className='SearchBox__result__name'>
                      {term.name}
                    </span>
                    <span className='SearchBox__result__id'>{term.id}</span>
                  </div>
                )
              }
              {
                terms.length === 0 &&
                  <div
                    className='SearchBox__results__row SearchBox__results__row--message'
                  >
                    No result found.
                  </div>
              }
              {
                termsTotalCount > terms.length &&
                  <div
                    role='button'
                    className='SearchBox__results__row SearchBox__results__row--clickable SearchBox__results__row--message'
                    onClick={this.onClickLoadAll}
                  >
                    Terms 0-{terms.length} of {termsTotalCount} — Click here to load all
                  </div>
              }
            </div>
        }
      </div>
    )
  }
  }

export default SearchBox;
