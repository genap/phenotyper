import { Component } from 'inferno';

import * as api from '../helpers/api'
import Logo from './logo';
import SearchBox from './SearchBox';
import ResultTable from './ResultTable';
import Alert from './Alert';

class App extends Component {
  state = {
    isLoading: false,
    results: undefined,
    message: undefined,
  }

  onSelectTerm = (term) => {
    console.log(term)

    this.setState({ isLoading: true })
    api.search.byTerm(term.name)
    .then(results => {

      this.setState({ isLoading: false, results })
      console.log(results)
    })
    .catch(err => {
      this.setState({
        isLoading: false,
        message: err.message,
      })
    })
  }

  render() {
    const { isLoading, results, message } = this.state

    return (
      <div className='App'>
        <div className='App__content'>

          <div className='App__head'>
            <Logo className='App__logo' width='80' height='80' />

            <div className='App__title'>Welcome to Phenotyper</div>

            <SearchBox
              isLoading={isLoading}
              onSubmit={this.onSelectTerm}
            />

            {
              message &&
                <Alert danger>
                  { message }
                </Alert>
            }
          </div>
          <div className='App__results'>
            <ResultTable
              results={results}
            />
          </div>

        </div>
      </div>
    );
  }
  }

export default App;
