/*
 * ResultTable.js
 */

import { Component } from 'inferno'
import { set, lensPath } from 'ramda'

import Icon from './Icon'

class ResultTable extends Component {
  state = {
    expandedItems: {},
  }

  toggleGenes = (genes) => {
    if (this.state.expandedItems[genes]) {
      const expandedItems = { ...this.state.expandedItems }
      delete expandedItems[genes]
      this.setState({
        expandedItems: expandedItems
      })
    }
    else {
      this.setState({
        expandedItems: { ...this.state.expandedItems, [genes]: true }
      })
    }
  }

  toggleItem = (genes, index) => {
    if (this.state.expandedItems[genes + '___' + index]) {
      const expandedItems = { ...this.state.expandedItems }
      delete expandedItems[genes + '___' + index]
      this.setState({
        expandedItems: expandedItems
      })
    }
    else {
      this.setState({
        expandedItems: { ...this.state.expandedItems, [genes + '___' + index]: true }
      })
    }
  }

  isExpanded(genes, index = undefined) {
    if (index !== undefined)
      return (genes + '___' + index) in this.state.expandedItems
    return genes in this.state.expandedItems
  }

  render() {
    const { results } = this.props

    if (results === undefined)
      return null

    return (
      <div className='ResultTable'>
        <div className='ResultTable__row ResultTable__head'>
          <div className='ResultTable__icon' />
          <div className='ResultTable__source' />
          <div className='ResultTable__featureNames'>Feature Names</div>
          <div className='ResultTable__diseases'>Diseases</div>
          <div className='ResultTable__drugs'>Drugs</div>
          <div className='ResultTable__response'>Response</div>
          <div className='ResultTable__evidenceLabel'>Evidence Label</div>
        </div>
        <div className='ResultTable__table'>
          {
            results.map(({ genes, items }, index, _, ) => {
              const expanded = this.isExpanded(genes)

              return (
                <div className='ResultTable__group'>

                  <div
                    className={
                      'ResultTable__row--genes ResultTable__row ResultTable__row--clickable'
                      + (expanded ? '' : ' ResultTable__row--collapsed')
                    }
                    role='button'
                    onClick={() => this.toggleGenes(genes)}
                  >
                    <div className='ResultTable__genes'>
                      <Icon name='chevron-right' rotate={expanded ? 90 : 0}  /> {genes}
                    </div>
                    <div className='ResultTable__featureNames'>
                      {expanded ? null : summary(items, 'featureNames')}
                    </div>
                    <div className='ResultTable__diseases'>
                      {expanded ? null : summary(items, 'diseases')}
                    </div>
                    <div className='ResultTable__drugs'>
                      {expanded ? null : summary(items, 'drugs')}
                    </div>
                    <div className='ResultTable__response'>
                      {expanded ? null : summary(items, 'response')}
                    </div>
                    <div className='ResultTable__evidenceLabel'>
                      {expanded ? null : summary(items, 'evidenceLabel')}
                    </div>
                  </div>

                  {
                    expanded &&
                      <div className='ResultTable__group__content'>
                      {
                        items.map((item, index, _, expanded = this.isExpanded(genes, index)) =>
                          <>
                            <div
                                className='ResultTable__row ResultTable__row--clickable'
                                role='button'
                                onClick={() => this.toggleItem(genes, index)}
                            >
                              <div className='ResultTable__icon'>
                                <Icon name='chevron-right' rotate={expanded ? 90 : 0}  />
                              </div>
                              <div className='ResultTable__source text-muted'>
                                {item.source}
                              </div>
                              <div className='ResultTable__featureNames'>{item.featureNames}</div>
                              <div className='ResultTable__diseases'>
                                {expanded ? item.diseases : item.diseases.slice(0, 200)}
                              </div>
                              <div className='ResultTable__drugs'>{item.drugs}</div>
                              <div className='ResultTable__response'>{item.response}</div>
                              <div className='ResultTable__evidenceLabel'>{item.evidenceLabel}</div>
                            </div>

                            {
                              expanded &&
                                <>
                                  <table className='ResultTable__details'>
                                  <tbody>
                                    <tr>
                                      <th><b>Description:</b></th>
                                      <td dangerouslySetInnerHTML={{ __html: highlight(item.description[0]) }}></td>
                                    </tr>
                                    <tr>
                                      <th><b>Source:</b></th>
                                      <td><a href={item.sourceURL}>{item.sourceURL}</a></td>
                                    </tr>
                                    <tr>
                                      <th><b>Publications:</b></th>
                                      <td>{
                                        item.publications &&
                                          <a href={item.publications}>{item.publications}</a>
                                      }</td>
                                    </tr>
                                  </tbody>
                                  </table>
                                  <div className='ResultTable__separator'/>
                              </>
                            }
                          </>
                        )
                      }
                      </div>
                  }
                  <div className='ResultTable__separator'/>
                </div>
              )
            })
          }
        </div>
      </div>
    )
  }
}

function summary(items, property) {
  return items.map(item => item[property]).filter(Boolean).join(', ').slice(0, 100)
}

function highlight(description) {
  return description.replace(/@kibana-highlighted-field@/g, '<b><i>')
                    .replace(/@\/kibana-highlighted-field@/g, '</i></b>')
}


/* {
 *   "source": "civic",
 *   "sourceURL": "https://civicdb.org/events/genes/6/summary/variants/185/summary/evidence/1370/summary#evidence",
 *   "genes": [
 *     "BRCA1"
 *   ],
 *   "drugs": "OLAPARIB",
 *   "featureNames": "EID1370",
 *   "publications": "http://www.ncbi.nlm.nih.gov/pubmed/19553641",
 *   "diseases": "cancer",
 *   "description": [
 *     "In this phase 1 trial, 60 patients with advanced solid tumors (ovarian, breast, colorectal, melanoma, @kibana-highlighted-field@sarcoma@/kibana-highlighted-field@, prostate) were treated with PARP inhibitor olaparib. Durable antitumor activity was only observed in BRCA1/2 mutation carriers. 19 BRCA carriers had ovarian, breast, or prostate cancers; 12 of these (63%) had a clinical benefit from treatment with olaparib: radiologic or tumor-marker responses or meaningful disease stabilization (stable disease for a period of 4 months or more). Nine BRCA carriers had a response according to RECIST, with the response sustained for more than 76 weeks in one patient. No responses were seen in patients without BRCA mutation."
 *   ],
 *   "evidenceLabel": "A",
 *   "response": "Sensitivity"
 * } */

export default ResultTable;
