/*
 * Icon.js
 */


function Icon({ name, className, spin, size, color, info, success, warning, danger, rotate, ...rest }, ref) {
  const iconClassName = (
      `Icon fa fa-${name}`
    + (className ? ' ' + className : '')
    + (size ?    ` fa-${size}` : '')
    + (color ?   ` text-${color}` : '')
    + (spin ?    ' fa-spin' : '')
    + (info ?    ' text-info' : '')
    + (success ? ' text-success' : '')
    + (warning ? ' text-warning' : '')
    + (danger ?  ' text-danger' : '')
  )

  const style = {
    transform: rotate !== undefined ? `rotate(${rotate}deg)` : undefined,
  }

  if (rest.onClick) {
    return (
      <button ref={ref} className={iconClassName} style={style} {...rest} />
    )
  }

  return (
    <i ref={ref} className={iconClassName} style={style} {...rest} />
  )
}

export default Icon
