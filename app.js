'use strict'

const path = require('path')
const AutoLoad = require('fastify-autoload')
const Static = require('fastify-static')

module.exports = function (fastify, opts, next) {
  // Place here your custom code!

  // Do not touch the following lines

  // This loads all plugins defined in plugins
  // those should be support plugins that are reused
  // through your application
  fastify.register(AutoLoad, {
    dir: path.join(__dirname, 'plugins'),
    options: Object.assign({}, opts)
  })


  // Public files
  fastify.register(Static, { root: path.join(__dirname, 'public') })
  fastify.register(require('./routes/root'),   { prefix: '/' })

  // Routes
  fastify.register(require('./routes/index'),  { prefix: '/' })
  fastify.register(require('./routes/search'), { prefix: '/api/search' })

  next()
}
