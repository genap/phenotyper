/*
 * do.js
 */


const request = require('request-promise-native')
const fuzzaldrin = require('fuzzaldrin')

const URL = 'https://github.com/DiseaseOntology/HumanDiseaseOntology/raw/master/src/ontology/doid-non-classified.json'
const PREFIX = 'http://purl.obolibrary.org/obo/'

module.exports = {
  search,
}



let terms = undefined
let termsResolve
let termsPromise = new Promise((resolve) => { termsResolve = resolve }).then(() => { return terms })

fetchTerms()
setInterval(fetchTerms, 24 * 60 * 60e3)


// /search?q=eye&fetchAll=true
function search(q, fetchAll = false) {
  return termsPromise.then(() => {

    const matches = []

    for (let i = 0; i < terms.length; i++) {
      const term = terms[i]
      const score = getScore(term, q)
      if (score > 0) {
        matches.push({ score, term })
      }
    }

    matches.sort((a, b) => b.score - a.score)

    const total = matches.length

    if (!fetchAll)
      matches.splice(10)

    const results = matches.map(m => m.term)

    return {
      terms: results,
      total: total,
    }
  })
}

// Helpers

function getScore(term, q) {
  let score = fuzzaldrin.score(term.name, q)
  for (let i = 0; i < term.synonyms.length; i++) {
    let otherScore = fuzzaldrin.score(term.synonyms[i])
    if (otherScore > score)
      score = otherScore
  }
  return score
}

function fetchTerms() {
  console.log('Fetching terms...')
  return request({ url: URL, json: true })
    .then(ontologyToTerms)
    .then(result => {
      console.log('Fetching terms: done')
      terms = result
      termsResolve(terms)
    })
}

function ontologyToTerms(ontology) {
  const nodes = ontology.graphs[0].nodes
  return nodes
    .filter(n => n.id.startsWith(PREFIX))
    .map(n => ({
      id: n.id.replace(PREFIX, ''),
      name: n.lbl,
      synonyms: ((n.meta || {}).synonyms || []).filter(x => x.pred === 'hasExactSynonym').map(x => x.val)
    }))
}
