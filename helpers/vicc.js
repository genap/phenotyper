/*
 * vicc.js
 */

const fetch = require('isomorphic-fetch')
const qs = require('qs')

const BASE = 'https://search.cancervariants.org/_plugin/kibana'

module.exports = {
  search,
}

// /elasticsearch/_msearch
function search(term) {
  const headers = {
    accept: 'application/json, text/plain, */*',
    'cache-control': 'no-cache',
    'content-type': 'application/x-ndjson',
    'kbn-version': '6.4.2',
    pragma: 'no-cache'
  }

  const searchOptions = {
    index: 'associations',
    ignore_unavailable: true,
    timeout: 30000,
    preference: 1556825707011
  }

  const searchQuery = {
    version: true,
    size: 500,
    sort: [{ evidence_label: { order: 'asc', unmapped_type: 'boolean' } }],
    _source: { excludes: [] },
    stored_fields: ['*'],
    script_fields: {},
    docvalue_fields: [],
    query: {
      bool: {
        must: [
          {
            query_string: {
              query: '*',
              analyze_wildcard: true,
              default_field: '*',
              default_operator: 'AND'
            }
          },
          {
            query_string: {
              analyze_wildcard: true,
              query: term,
              default_field: '*',
              default_operator: 'AND'
            }
          },
          {
            query_string: {
              analyze_wildcard: true,
              default_field: '*',
              query: '-source:*trials AND -source:sage AND -source:brca',
              default_operator: 'AND'
            }
          }
        ],
        filter: [],
        should: [],
        must_not: []
      }
    },
    highlight: {
      pre_tags: ['@kibana-highlighted-field@'],
      post_tags: ['@/kibana-highlighted-field@'],
      fields: { '*': {} },
      fragment_size: 2147483647
    }
  }

  const body = JSON.stringify(searchOptions) + '\n' + JSON.stringify(searchQuery) + '\n'

  return fetch(BASE + '/elasticsearch/_msearch', {
    credentials: 'omit',
    headers: headers,
    referrer:'https://search.cancervariants.org/_plugin/kibana/app/kibana',
    referrerPolicy:'no-referrer-when-downgrade',
    body: body,
    method: 'POST',
    mode: 'cors'
  })
  .then(res => res.json())
  .then(data => {
    const { responses: [ { status, hits } ] } = data
    // const response = responses[0]
    // const status = response.status
    if (status !== 200)
      return Promise.reject(new Error('Response status is different than 200: ' + status))

    // { hits: Object[], total: Number }
    return hits
  })
}
