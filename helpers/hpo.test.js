/*
 * hpo.test.js
 */


const hpo = require('./hpo')

jest.setTimeout(30 * 1000)

describe('hpo', () => {

  it('.search()', async () => {
    const response = await hpo.search('eye')
    expect(response.terms).toBeDefined()
    expect(response.diseases).toBeDefined()
    expect(response.genes).toBeDefined()
  })

  it('.genes()', async () => {
    const response = await hpo.genes('HP:0012174', { offset: 0, max: 20 })
    expect(response.genes).toBeDefined()
    expect(response.offset).toBe(0)
    expect(response.max).toBe(20)
    expect(response.geneCount).toBeLessThanOrEqual(20)
  })
})
