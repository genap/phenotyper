/*
 * hpo.js
 */


const request = require('request-promise-native')
const qs = require('qs')

const BASE = 'https://hpo.jax.org/api/hpo'

module.exports = {
  search,
  genes,
}

// /search?q=eye&fetchAll=true
function search(q, fetchAll = false) {
  return fetch('/search', { params: { q, fetchAll } })
}

// /term/HP:0012174/genes?offset=0&max=20
function genes(term, { offset = 0, max = 20 } = {}) {
  return fetch(`/term/${term}/genes`, { params: { offset, max } })
}

// Helpers

function fetch(route, options) {
  const params = options.params
  const url = BASE + route + (params ? '?' + qs.stringify(params) : '')
  return request({ url, json: true, ...options })
}
