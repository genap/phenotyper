/*
 * vicc.test.js
 */


const vicc = require('./vicc')

jest.setTimeout(30 * 1000)

describe('vicc', () => {

  it('.search()', async () => {
    const response = await vicc.search('sarcoma')
    expect(response.hits).toBeDefined()
    expect(response.total).toBeDefined()
  })
})
