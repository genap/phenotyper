'use strict'

const fp = require('fastify-plugin')

// the use of fastify-plugin is required to be able
// to export the decorators to the outer scope

module.exports = fp(function (fastify, opts, next) {

  fastify.decorateReply('ok', function (data) {
    this.send({ ok: true, data })
  })

  fastify.decorateReply('error', function (message, stack = []) {
    if (message instanceof Error) {
      stack = message.stack.split('\n')
      message = message.message
    }

    this.send({ ok: false, message, stack })
  })

  fastify.decorateReply('okHandler', function () {
    return (data) => this.ok(data)
  })

  fastify.decorateReply('errorHandler', function () {
    return (message, stack) => this.error(message, stack)
  })

  next()
})

// If you prefer async/await, use the following
//
// module.exports = fp(async function (fastify, opts) {
//   fastify.decorate('someSupport', function () {
//     return 'hugs'
//   })
// })
