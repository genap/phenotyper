/*
 * search.js
 */


const DO = require('../helpers/do.js')
const vicc = require('../helpers/vicc.js')

module.exports = {
  autocomplete,
  byTerm,
}

function autocomplete(input, fetchAll = false) {
  return DO.search(input, fetchAll)
}

function byTerm(term) {
  return vicc.search(term)
}
